package com.nlmk.potapov.jse25;

public class CSVUtils {

    private static final String DELIMITER = ";";

    private CSVUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static String format(String value) {
        StringBuilder formattedString = new StringBuilder(value);
        int index = -1;
        while ((index = formattedString.indexOf("\"", index + 1)) != -1) {
            formattedString.insert(index, "\"");
            index++;
        }
        if (formattedString.indexOf(DELIMITER) != -1) {
            formattedString.insert(0, "\"");
            formattedString.insert(formattedString.length(), "\"");
        }
        formattedString.append(DELIMITER);
        return formattedString.toString();
    }

}
