package com.nlmk.potapov.jse25;

import java.io.*;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;

public class Serializer {

    public void serialize(List<Object> objects, String fileName) {

        Class<?> listObjectsClass = objects.get(0).getClass();
        for (Object object : objects) {
            if (!Objects.equals(object.getClass(), listObjectsClass)) {
                throw new IllegalArgumentException();
            }
        }

        try (BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(new File(fileName + ".csv")))) {
            byte[] bytes = serializeHeader(listObjectsClass).getBytes();
            bufferedOutputStream.write(bytes);
            for (Object object : objects) {
                bytes = serializeRow(object).getBytes();
                bufferedOutputStream.write(bytes);
            }
            bufferedOutputStream.flush();
        } catch (FileNotFoundException e) {
            System.out.println("Нет доступа к файлу: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Ошибка ввода/вывода: " + e.getMessage());
        }
    }

    private String serializeHeader(Class<?> clazz) {
        StringBuilder headerString = new StringBuilder();
        for (Field field : clazz.getDeclaredFields()) {
            headerString.append(CSVUtils.format(field.getName()));
        }
        headerString.append("\n");
        return headerString.toString();
    }

    private String serializeRow(Object object) {
        Class<?> objectClass = object.getClass();
        StringBuilder rowString = new StringBuilder();
        for (Field field : objectClass.getDeclaredFields()) {
            field.setAccessible(true);
            try {
                rowString.append(CSVUtils.format(field.get(object).toString()));
            } catch (IllegalAccessException e) {
                System.out.println("Ошибка при попытке получить значение поля: " + e.getMessage());
            }
        }
        rowString.append("\n");
        return rowString.toString();
    }

}
