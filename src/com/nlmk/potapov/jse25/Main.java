package com.nlmk.potapov.jse25;

import java.time.LocalDate;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Person p1 = new Person("John", "Doe", LocalDate.parse("1991-06-14"), "anonymous@gmail.com");
        Person p2 = new Person("Billie", "Jean", LocalDate.parse("1983-01-02"), "mjack@gmail.com");

        new Serializer().serialize(Arrays.asList(p1, p2), "out");
    }
}
